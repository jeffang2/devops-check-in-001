#!/bin/bash

echo "Initiating other services in the background"
sh /tmp/wait-for-other.sh &

echo "Running Pinger"
/bin/pinger
