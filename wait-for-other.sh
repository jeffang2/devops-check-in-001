#!/bin/bash
set -e

until curl --output /dev/null --silent --head --fail "$TARGET_URL"; do
#  >&2 echo "Service is unavailable - sleeping"
  sleep 1
done
>&2 echo "Service is up"

while true
do
  curl http://$TARGET_URL
  sleep 1
done

