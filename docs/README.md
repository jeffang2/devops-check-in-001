This document consists detail of each components to help admin performs each task as stated on the main README.md.

# Key components

## pinger
The main app of this project. All it does is listening to incoming traffic on the listening port(default: 8000)
binary path: /bin/pinger (after compiled using Go)
source code: /cmd/pinger 

## Dockerfile
File used for building an docker image
path: ./deployments/build/Dockerfile

Content of Dockerfile:
```
FROM alpine:latest      <-- Docker image used
ADD ./bin/pinger /bin   <-- Add pinger binary to /bin inside the image
ADD ./wait-for-other.sh /tmp    <-- Add bash script to /tmp inside the image
ADD ./startup.sh /tmp           <-- (same as above)
RUN apk add --no-cache libc6-compat curl    <-- Add required libraries to support shell runs and curl function
CMD ["sh","/tmp/startup.sh"]    <-- Command executes once docker is up
```

Expected result:
```
Sending build context to Docker daemon     29MB
Step 1/6 : FROM alpine:latest
 -ep 3/6 : ADD ./wait-for-other.sh /tmp
 ---> 28f6e2705743
Step 2/6 : ADD ./bin/pinger /bin
 ---> Using cache
 ---> 36a344622b48
St--> Using cache
 ---> 315e420799e0
Step 4/6 : ADD ./startup.sh /tmp
 ---> Using cache
 ---> ff70eec8a9d8
Step 5/6 : RUN apk add --no-cache libc6-compat curl
 ---> Using cache
 ---> 10b23307532a
Step 6/6 : CMD ["sh","/tmp/startup.sh"]
 ---> Using cache
 ---> 68a57d019373
Successfully built 68a57d019373
Successfully tagged devops/pinger:latest

```

## Docker-compose.yml
A YML file for deploying docker images and container and it is designed to build image from Dockerfile.  
path: ./deployments/docker-compose.yml

Example command:
```
docker build -f ./deployments/build/Dockerfile -t devops/pinger:latest .
```

Content of docker-compose.yml:
```
version: "3.9"

services:
  pinger1:
    build:
      context: ../
      dockerfile: ./deployments/build/Dockerfile
    ports:
      - "8000:8000"
    environment:
      TARGET_URL: "pinger2:8000"    <-- Set environment variable as other pinger, so curl shell script know who to call.
  pinger2:
    build:
      context: ../
      dockerfile: ./deployments/build/Dockerfile
    ports:
      - "9000:8000"
    environment:
      TARGET_URL: "pinger1:8000"    <-- Set environment variable as other pinger, so curl shell script know who to call.
```

Example command:
```
docker-compose -f deployments/docker-compose.yml up
```

Expected result:
```
Building with native build. Learn about native build in Compose here: https://docs.docker.com/go/compose-native-build/
Starting deployments_pinger1_1 ... done
Starting deployments_pinger2_1 ... done
Attaching to deployments_pinger1_1, deployments_pinger2_1
pinger1_1  | Initiating other services in the background
pinger1_1  | Running Pinger
pinger1_1  | service|2021/02/23 14:17:33 initialising service...
pinger1_1  | service|2021/02/23 14:17:33 attempting to listen on '0.0.0.0:8000'...
pinger2_1  | Initiating other services in the background
pinger2_1  | Running Pinger
pinger2_1  | service|2021/02/23 14:17:33 initialising service...
pinger2_1  | service|2021/02/23 14:17:33 attempting to listen on '0.0.0.0:8000'...
...

Should be expecting something like this...
server|2021/02/23 14:17:33 < pinger2:8000 <- 172.22.0.2:45138 | HTTP/1.1 GET /  <-- This means this IP(assume to be pinger1) is calling pinger2
server|2021/02/23 14:17:33 < pinger1:8000 <- 172.22.0.3:52662 | HTTP/1.1 GET /  <-- This means this IP(assume to be pinger2) is calling pinger1
```

# gitlab CICD 
**Do take note that tests only take into account of test related to compile of builds and not functional. Also no action needed as CICD test is auto-triggered whenever there is an update to the repo. Just keeping an eye on the result.


path: ./gitlab-ci.yml

CICD breakdowns into 3 stages as listed below:

## STAGES
### 1. build-pinger

Compliles a pinger build and creates the pinger artifacts from a Golang image.

### 2. build-docker

Builds docker image based on the Dockerfile and creates an artifacts based on docker image.

### 3. testbuild

Grab the docker artifacts and try build it.


# NEW ADDED FILES
## startup.sh

This is a startup bash script for running pinger service and a sub-script: wait-for-other.sh

## wait-for-other.sh

An sub-script running at the background for checking the realiness of other pinger. It will perform infinite curl to other pinger.

